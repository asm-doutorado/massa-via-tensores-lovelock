[![](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](./LICENSE.md)

# :point_up: [Projeto descontinuado](./README.md#conclusão)

# Projeto: massa via tensores de Lovelock

Em um trabalho recente ([Evaluation of the ADM mass and center of mass via Ricci
tensor](http://arxiv.org/abs/1408.3893)) Pengzi Miao e Luen-Fai Tam mostraram
como calcular a massa ADM de uma variedade Riemannina assintoticamente plana à
partir do seu tensor de Ricci, reobtendo uma (já conhecida) [relação entre a
massa ADM e o tensor de Einstein (tensor de Lovelock de primeira
ordem)](http://arxiv.org/pdf/1408.3893v2.pdf#thm.1.1).

### Nossos planos

Pretendemos investigar se uma relação semelhante àquela obtida por Pengzi Miao e
Luen-Fai Tam é válida para a [r-massa de Gauss-Bonnet-Chern](http://arxiv.org/abs/1211.3645)
e o r-ésimo tensor de Lovelock.

### Conclusão

No trabalho [ Chern's magic form and the Gauss-Bonnet-Chern mass](http://arxiv.org/abs/1510.03036),
os autores (Guofang Wang, Jie Wu) mostraram que a questão que pretendíamos investigar tem resposta
positiva. Dessa forma, esse projeto não tem mais razão para existir. Logo, foi descontinuado.